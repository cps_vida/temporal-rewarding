"""Defines a trial where we compare a vanilla implementation against a TLAgent"""

import copy

import gym
import numpy as np

from temporal_rl.session import Session
from temporal_rl.rl.agents import BaseAgent, TLAgent


class Trial:

    def __init__(self,
                 vanilla_agent: BaseAgent,
                 tl_agent: TLAgent,
                 session: Session,
                 n_trials: int,
                 n_episodes: int,
                 save_dir: str,
                 **kwargs):
        self.vanilla = vanilla_agent
        self.tl_agent = tl_agent
        self.session = session
        self.n_trials = n_trials
        self.n_episodes = n_episodes

        self.save_dir = save_dir
        self.episode_len = kwargs.get('episode_len', 500)

        self.vanilla_rewards = None
        self.temporal_rewards = None

    def check_normal_agent(self):
        exp = self.session
        rewards = np.zeros((self.n_trials, self.n_episodes))
        for trial in range(self.n_trials):
            exp.reset()


