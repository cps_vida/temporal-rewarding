import temporal_rl.temporal_logic.signal_tl as stl
from temporal_rl.temporal_logic.signal_tl import signals, G, F

SIGNALS = ('x', 'x_dot', 'theta', 'theta_dot')
x, x_dot, theta, theta_dot = signals(SIGNALS)

VELOCITY_SPEC = F(abs(x_dot) < 0.1)
POSITION_SPEC = abs(x) < 0.5
ANGLE_SPEC = abs(theta) < 5

SAFETY_SPEC = G(VELOCITY_SPEC & POSITION_SPEC & ANGLE_SPEC)


