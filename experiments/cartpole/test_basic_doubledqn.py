"""
In this test, we are going to compare the performance of 2 Deep RL agents in the CartPole env. Specifically, we are
going to compare Double DQN agents with and without STL reward shaping. Both these agents use uniform episodic sampling.
The reason not to use some form of prioritized experience replay is to check how much better STL reward shaping performs
compared to vanilla Double DQN.
"""

import copy

import gym
import torch.nn as nn
import numpy as np

import plotly.offline as py
import plotly.graph_objs as go

from temporal_rl import Session
from temporal_rl.rl.agents import TLAgent, DoubleDQNAgent

import temporal_rl.temporal_logic.signal_tl as stl
from temporal_rl.temporal_logic.signal_tl.semantics import FilteringMonitor

from .specifications import SAFETY_SPEC

import os

from experiments.utils.plotting import get_mean_plot

NET = nn.Sequential(
    nn.Linear(4, 24), nn.ReLU(),
    nn.Linear(24, 24), nn.ReLU(),
    nn.Linear(24, 2)
)

#SAVE_DIR = 'data/cartpole/basic_doubledqn/'
SAVE_DIR = '/home/anand/Dropbox/TemporalRL/data/cartpole/basic_doubledqn/'


def check_normal_agent(trials=1000, n_episodes=2000):
    print("================================================================================")
    print("[RUNNING] Vanilla Double DQN ")
    rewards = np.zeros((trials, n_episodes))
    durations = np.zeros((trials, n_episodes))
    save_dir = os.path.join(SAVE_DIR, 'vanilla')
    for trial in range(trials):
        print(
            '------------------------------------------------------> Vanilla {:04d}/{:04d}'.format(trial, trials))
        exp = Session(gym.make('CartPole-v1'))
        module = copy.deepcopy(NET)
        agent = DoubleDQNAgent(4, 2, module)
        exp.run(agent, n_episodes, render=False,
                save_dir=save_dir, monitor=True)
        rewards[trial, :] = exp.np_rewards()
        durations[trial, :] = exp.durations

    np.save(os.path.join(save_dir, 'trial_rewards'), rewards)

    r_bounds, r_mean = get_mean_plot(rewards, 'No STL Spec')
    d_bounds, d_mean = get_mean_plot(durations, 'No STL Spec')
    return dict(
        r_bounds=r_bounds,
        r_mean=r_mean,
        d_bounds=d_bounds,
        d_mean=d_mean,
    )


def check_tl_agent(trials=1000, n_episodes=2000):
    print("================================================================================")
    print("[RUNNING] Double DQN + STL Spec ")

    signals = ('x', 'x_dot', 'theta', 'theta_dot')
    x, x_dot, theta, theta_dot = stl.signals(signals)
    spec = SAFETY_SPEC

    rewards = np.zeros((trials, n_episodes))
    durations = np.zeros((trials, n_episodes))
    save_dir = os.path.join(SAVE_DIR, 'plus_stl')
    for trial in range(trials):
        print(
            '------------------------------------------------------> TLAgent {:04d}/{:04d}'.format(trial, trials))
        exp = Session(gym.make('CartPole-v1'))
        module = copy.deepcopy(NET)

        tlmonitor = FilteringMonitor(spec, signals)
        base_agent = DoubleDQNAgent(4, 2, module)
        agent = TLAgent(base_agent, tlmonitor, horizon=10)

        exp.run(agent, n_episodes, render=False,
                save_dir=save_dir, monitor=True)
        rewards[trial, :] = exp.np_rewards()
        durations[trial, :] = exp.durations

    np.save(os.path.join(save_dir, 'trial_rewards'), rewards)

    r_bounds, r_mean = get_mean_plot(
        rewards, r'$ \varphi = {spec}$'.format(spec=spec.tex_print()))
    d_bounds, d_mean = get_mean_plot(
        durations, r'$ \varphi = {spec}$'.format(spec=spec.tex_print()))
    return dict(
        r_bounds=r_bounds,
        r_mean=r_mean,
        d_bounds=d_bounds,
        d_mean=d_mean,
    )


def run():
    v_stats = check_normal_agent(100, 1000)
    tl_stats = check_tl_agent(100, 1000)

    data_durations = [
        # v_stats['d_bounds'],
        # tl_stats['d_bounds'],
        v_stats['d_mean'],
        tl_stats['d_mean'],
    ]
    layout_d = go.Layout(
        title='Average Duration across trials',
        xaxis=dict(
            title='Episode #'
        ),
        yaxis=dict(
            title='Average Duration'
        )
    )

    data_rewards = [
        # v_stats['r_bounds'],
        # tl_stats['r_bounds'],
        v_stats['r_mean'],
        tl_stats['r_mean'],
    ]
    layout_r = go.Layout(
        title='Average Total Reward across trials',
        xaxis=dict(
            title='Episode #'
        ),
        yaxis=dict(
            title='Total Rewards'
        )
    )

    fig_d = go.Figure(data=data_durations, layout=layout_d)
    fig_r = go.Figure(data=data_rewards, layout=layout_r)
    save_file_d = os.path.join(SAVE_DIR, 'durations_plot.html')
    save_file_r = os.path.join(SAVE_DIR, 'rewards_plot.html')
    py.plot(fig_d, filename=save_file_d)
    py.plot(fig_r, filename=save_file_r)


if __name__ == '__main__':
    run()
