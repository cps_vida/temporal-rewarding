"""Utilities for Plottting coomming Deep RL stuff"""

import plotly.offline as py
import plotly.graph_objs as go
import numpy as np


def get_mean_plot(d, name):
    rewards = np.asarray(d)
    y = np.mean(rewards, axis=0)
    x = np.arange(len(y))
    x_rev = x[::-1]
    std = np.std(rewards, axis=0)
    upper = y + std
    lower = (y - std)[::-1]

    trace_mean = go.Scatter(
        x=x,
        y=y,
        name=name,
        mode='lines',
    )

    trace_bounds = go.Scatter(
        x=x + x_rev,
        y=upper + lower,
        fill='tozerox',
        name=name,
        showlegend=False,
    )

    return trace_bounds, trace_mean


