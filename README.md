# Temporal Logics + Reinforcement Learning

In this repository we provide a package for incorporating temporal logics (currently only STL) and reinforcement learning. The package includes popular Deep RL algorithms implemented using PyTorch, and an interface to define STL formulas in a Pythonic manner. The `experiments` directory includes a scripts that can be used to run predefined experiments that we use to test our models.

## Requirements

- Python >= 3.3

To get the latest Python installation, use a tool like [pyenv](https://github.com/pyenv/pyenv) or [Conda](https://conda.io/docs/).


## Setup

### (Optional) GPU support

Install a [CUDA](https://developer.nvidia.com/cuda-downloads) version compatible with PyTorch (includes CUDA 9 and CUDA 10).

### (Recommended) Using Anaconda (or Miniconda)

With the latest `conda`, clone this repo and run:

```
conda env create -f environment.yml
```

### Using Pip (Virtualenv or without)

To use a throwaway virtualenv, follow [this guide](https://docs.python.org/3/tutorial/venv.html).

```
pip install -e .
```

## Running experiments

To run an experiment in the `experiments` directory, simply run

```
python3 <path/to/script.py>
```

For example, to run the comparison between Double DQN and STL+Double DQN in the CartPole environment, run 

```
python3 experiments/cartpole/test_basic_doubledqn.py
```


