import os

from collections import deque

import gym
from gym.wrappers import Monitor

import numpy as np

from temporal_rl.rl.agents import BaseAgent


class Session:
    """Defines an Session"""

    def __init__(self, environment):
        self._env: gym.Env = environment
        self.env = self._env
        self.episode_count = 0
        self._rewards = deque()
        self._durations = deque()

    def reset(self):
        self.episode_count = 0
        self._rewards = deque()

    @property
    def rewards(self):
        return self._rewards

    def np_rewards(self):
        return np.array(self.rewards)

    @property
    def durations(self):
        return self._durations

    def run(self, agent, n_episodes, batch_size=32, save_dir=None, monitor=False, render=False):
        """
        Run an `n` episode experiment with the given agent.

        :param agent: Reinforcement learning agent
        :type agent: BaseAgent
        :param n_episodes: Number of episodes to run for the experiment
        :type n_episodes: int
        :param batch_size: Batch size for replay
        :type batch_size: int
        :param save_dir: Save data at directory
        :param monitor: Monitor the experiment at the specified directory (need to specify save_dir)
        :type monitor: bool
        :param render: Render the experiment
        :type render: bool
        """

        if save_dir:
            os.makedirs(os.path.join(save_dir, 'data'), exist_ok=True)
            os.makedirs(os.path.join(save_dir, 'monitor'), exist_ok=True)
            if monitor:
                self.env = Monitor(self._env, os.path.join(save_dir, 'monitor'), force=True)

        for episode in range(n_episodes):
            duration, total_reward = self.run_episode(agent, batch_size=batch_size, render=render)
            self.episode_count += 1
            self.rewards.append(total_reward)
            self.durations.append(duration)
            print('[{:{width}d}/{:d}] Duration: {:6d} | Score: {:4.4f}'
                  .format(episode, n_episodes, duration, total_reward, width=len(str(n_episodes))))

        if save_dir:
            rewards_buffer = np.array(self.rewards)
            np.save(os.path.join(save_dir, 'rewards'), rewards_buffer)
            agent.save(os.path.join(save_dir, 'model'), agent.policy_net)

        self.env = self._env
        self.env.close()

    def run_episode(self, agent, batch_size=32, render=False):
        """
        Run an episode with the given agent. Returns the duration of the episode.
        :param agent: Reinforcement learning agent.
        :type agent: BaseAgent
        :param batch_size: Batch size for replay
        :type batch_size: int
        :param render: Render the OpenAI Gym episode
        :type render: bool
        :return: Duration of the episode
        :rtype: int
        """
        total_reward = 0
        state = self.env.reset()

        t = 0
        done = False
        while not done:
            if render:
                self.env.render()
            action = agent.act(state)
            obs, reward, done, _ = self.env.step(action)
            agent.remember(state, action, reward, obs, done)
            agent.update_step()
            total_reward += reward
            t += 1
            state = obs
            output, target = agent.replay(batch_size)
            agent.learn(output, target)
        agent_tracked_reward = agent.get_episodic_reward()
        agent.update_post_episode()
        if agent_tracked_reward is not None:
            total_reward = agent_tracked_reward
        return t, total_reward
