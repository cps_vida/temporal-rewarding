"""DQN with fixed target updates"""
import copy

import torch
from torch import nn

from .dqn import DQNAgent
from ..utils.memory import Memory


class DQNFixedTargetAgent(DQNAgent):

    def __init__(self,
                 n_inputs: int, n_outputs: int,
                 module: nn.Module,
                 **kwargs):
        super(DQNFixedTargetAgent, self).__init__(n_inputs, n_outputs, module, **kwargs)

        self.target_update = kwargs.get('target_update', 10)
        self.target_net = copy.deepcopy(module).double().to(self.device)  # type: nn.Module
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()

        self.target_update_count = 0

    def remember(self, state, action, reward, next_state, done):
        super(DQNFixedTargetAgent, self).remember(state, action, reward, next_state, done)

    def update_step(self):
        super(DQNFixedTargetAgent, self).update_step()
        self.target_update_count += 1
        if self.target_update_count % self.target_update == 0:
            self.target_net.load_state_dict(self.policy_net.state_dict())

    def _q_update(self, transition):
        states, actions, rewards, next_states, final_states = transition
        q_max = torch.max(self.target_net(next_states), 1)[0].detach()
        target_value = rewards + (self.gamma * q_max)
        return torch.where(final_states, rewards, target_value).unsqueeze(1)
