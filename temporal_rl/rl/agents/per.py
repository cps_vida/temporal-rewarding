"""
Prioritized Experience Replay [1]

[1]:    Tom Schaul, John Quan, Ioannis Antonoglou, David Silver. "Prioritized Experience Replay"
        https://arxiv.org/abs/1511.05952
"""

from .base_agent import BaseAgent
from ..utils import PrioritizedMemory

import torch


class PERAgent(BaseAgent):

    def update_post_episode(self):
        self.base_agent.update_post_episode()

    def update_step(self):
        self.base_agent.update_step()

    def __init__(self, agent: BaseAgent, **kwargs):
        """Initialize the PERAgent with the agent you want to build on top off
        :param agent: The base agent you want to run PER with
        """
        self.base_agent = agent
        capacity = agent.memory.capacity
        self.base_agent.memory = PrioritizedMemory(capacity)

    @property
    def memory(self) -> PrioritizedMemory:
        return self.base_agent.memory

    @memory.setter
    def memory(self, new_mem):
        self.base_agent.memory = new_mem

    @property
    def policy_net(self):
        return self.base_agent.policy_net

    @property
    def n_in(self):
        return self.base_agent.n_in

    @property
    def n_out(self):
        return self.base_agent.n_out

    @property
    def device(self):
        return self.base_agent.device

    def remember(self, state, action, reward, next_state, done):
        self.base_agent.remember(state, action, reward, next_state, done)

    def act(self, state):
        return self.base_agent.act(state)

    def replay(self, batch_size):
        return self.base_agent.replay(batch_size)

    def learn(self, predicted, target):
        if predicted is None or target is None:
            return
        err = torch.abs(predicted - target).detach()
        idxs, weights = self.memory.last_idx_weights
        for i, idx in enumerate(idxs):
            self.memory.update(idx, err[i].item())
        self.base_agent.learn(predicted, target)
