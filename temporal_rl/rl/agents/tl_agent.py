"""
This file implements a Temporal Logic Agent (Active Research)

"""
from collections import deque

import numpy as np

from .base_agent import BaseAgent
from temporal_rl.temporal_logic.signal_tl.semantics import BaseMonitor
from ..utils import Transition


class TLAgent(BaseAgent):
    memory_type = Transition

    def __init__(self,
                 agent: BaseAgent,
                 monitor: BaseMonitor,
                 **kwargs):
        self.base_agent = agent
        self._monitor = monitor
        self.horizon = kwargs.get('horizon', np.inf)

        tmp_horizon = self.horizon if not np.isinf(self.horizon) else None
        self.partial_signal = deque(maxlen=tmp_horizon)
        self._last_point = None
        self.time_step = 0

        self.episode_rewards = 0

    @property
    def monitor(self):
        return self._monitor

    @property
    def last_point(self) -> Transition:
        return self._last_point

    def _add_traj(self, transition: Transition):
        self.partial_signal.append(transition)
        self._last_point = transition

    def _flush(self):
        transitions = list(self.partial_signal)
        split_signal = Transition(*zip(*transitions))
        w = np.array(split_signal.state)
        R = np.zeros(len(self.partial_signal))
        if len(R) > 2:
            # if we have a large enough signal
            R = self.monitor(w)
        t_list = Transition(
            split_signal.state,
            split_signal.action,
            R,
            split_signal.next_state,
            split_signal.done
        )
        transitions = map(lambda t: Transition(*t), zip(*t_list))
        for tr in transitions:
            self.base_agent.remember(*tr)
        self.episode_rewards += np.sum(R)
        self.partial_signal.clear()

    def remember(self, state, action, reward, next_state, done):
        self._add_traj(Transition(state, action, reward, next_state, done))

    def act(self, state):
        return self.base_agent.act(state)

    def replay(self, batch_size):
        return self.base_agent.replay(batch_size)

    def learn(self, predicted, target):
        return self.base_agent.learn(predicted, target)

    def update_post_episode(self):
        self.base_agent.update_post_episode()
        # self._flush()
        self.episode_rewards = 0

    def update_step(self):
        self.base_agent.update_step()
        self.time_step += 1
        if self.last_point.done or self.time_step % self.horizon == 0:
            self._flush()

    @property
    def memory(self):
        return self.base_agent.memory

    @property
    def policy_net(self):
        return self.base_agent.policy_net

    @property
    def n_in(self):
        return self.base_agent.n_in

    @property
    def n_out(self):
        return self.base_agent.n_out

    @property
    def device(self):
        return self.base_agent.device

    def get_episodic_reward(self):
        return self.episode_rewards
