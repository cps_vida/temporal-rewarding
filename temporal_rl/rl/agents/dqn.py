"""Deep Q-Learning Agent"""

import random

from abc import abstractmethod

import numpy as np
import torch
import torch.nn.functional as F
from torch import nn, optim

from .base_agent import BaseAgent
from ..utils.memory import Memory, Transition


class DQNAgent(BaseAgent):
    """Deep Q-Learning Agent"""

    memory_type = Transition

    def __init__(self,
                 n_inputs: int, n_outputs: int,
                 module: nn.Module,
                 **kwargs):
        self._n_inputs = n_inputs
        self._n_outputs = n_outputs
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self._device = kwargs.get('device', device)

        self._policy_net = module.double().to(self.device)

        self.gamma = kwargs.get('gamma', 0.95)
        self.epsilon = kwargs.get('epsilon', 1.0)
        self.epsilon_min = kwargs.get('epsilon_min', 0.1)
        self.epsilon_decay = kwargs.get('epsilon_decay', 0.95)
        self.lr = kwargs.get('lr', 0.001)
        self.mem_capacity = kwargs.get('mem_capacity', 2000)

        self.loss_fn = kwargs.get('loss', F.mse_loss)
        self.optimizer = kwargs.get('optimizer', optim.Adam)(self.policy_net.parameters(), lr=self.lr)
        self._memory = kwargs.get('memory', Memory)(self.mem_capacity)

    @property
    def policy_net(self):
        return self._policy_net

    @property
    def memory(self):
        return self._memory

    @memory.setter
    def memory(self, new_mem):
        self._memory = new_mem

    @property
    def n_in(self):
        return self._n_inputs

    @property
    def n_out(self):
        return self._n_outputs

    @property
    def device(self):
        return self._device

    def remember(self, state, action, reward, next_state, done):
        self.memory.push(Transition(state, action, reward, next_state, done))

    def act(self, state):
        epsilon = self.epsilon
        if np.random.rand() <= epsilon:
            return random.randrange(self.n_out)
        with torch.no_grad():
            s = self._get_tensor(state)
            a = self.policy_net(s.unsqueeze(0))
            return torch.argmax(a[0]).item()

    def update_step(self):
        pass

    def replay(self, batch_size):
        if len(self.memory) < batch_size:
            return None, None

        tensor_batch = self._recall(batch_size)
        target_vector = self._q_update(tensor_batch)

        state_batch = tensor_batch.state
        action_batch = tensor_batch.action
        outputs = self.policy_net(state_batch).gather(1, action_batch.unsqueeze(1))
        return outputs, target_vector

    def _recall(self, batch_size):
        sample = self.memory.sample(batch_size)
        batch = self.memory_type(*zip(*sample))
        state_batch = self._get_tensor(batch.state)
        action_batch = self._get_tensor(batch.action, dtype=torch.long)
        reward_batch = self._get_tensor(batch.reward)
        next_state_batch = self._get_tensor(batch.next_state)
        final_states = self._get_tensor(batch.done, dtype=torch.uint8)

        return self.memory_type(state_batch, action_batch, reward_batch, next_state_batch, final_states)

    def _q_update(self, transition):
        states, actions, rewards, next_states, final_states = transition
        q_max = self.policy_net(next_states).max(1)[0].detach()
        target_value = rewards + (self.gamma * q_max)
        target_vector = torch.where(final_states, rewards, target_value)
        return target_vector.unsqueeze(1)

    def learn(self, predicted, target):
        if predicted is None or target is None:
            return
        self.optimizer.zero_grad()
        loss = self.loss_fn(predicted, target)
        loss.backward()
        self.optimizer.step()

    def update_post_episode(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= np.exp(-self.epsilon_decay)
