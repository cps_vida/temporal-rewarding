"""Double DQN"""

import copy

import torch
from torch import nn

from .dqn_fixed_target import DQNFixedTargetAgent
from ..utils.memory import Memory, Transition


class DoubleDQNAgent(DQNFixedTargetAgent):

    def __init__(self,
                 n_inputs: int, n_outputs: int,
                 module: nn.Module,
                 **kwargs):
        super(DoubleDQNAgent, self).__init__(n_inputs, n_outputs, module, **kwargs)

    def _q_update(self, transition: Transition):
        states, actions, rewards, next_states, final_states = transition
        policy_next_q = self.policy_net(next_states)
        argmax_policy_q = policy_next_q.max(1)[1].unsqueeze(1)
        target_next_q = self.target_net(next_states) \
            .gather(1, argmax_policy_q) \
            .squeeze(1)
        expected_target = rewards + self.gamma * target_next_q * ((1.0 - final_states).double())  # type: torch.Tensor
        return expected_target.unsqueeze(1).detach()
