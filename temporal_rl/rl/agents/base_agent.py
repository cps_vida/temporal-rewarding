"""Base Deep RL Agent

All agents must inherit from this.
"""

import os
from abc import ABC, abstractmethod

import torch


class BaseAgent(ABC):
    memory_type = None

    @property
    @abstractmethod
    def memory(self):
        pass

    @memory.setter
    @abstractmethod
    def memory(self, new_mem):
        pass

    @property
    @abstractmethod
    def policy_net(self):
        pass

    @property
    @abstractmethod
    def n_in(self):
        pass

    @property
    @abstractmethod
    def n_out(self):
        pass

    @property
    @abstractmethod
    def device(self):
        pass

    @abstractmethod
    def remember(self, state, action, reward, next_state, done):
        pass

    @abstractmethod
    def act(self, state):
        pass

    @abstractmethod
    def replay(self, batch_size):
        pass

    @abstractmethod
    def learn(self, predicted, target):
        pass

    @classmethod
    def save(cls, directory, policy_net):
        os.makedirs(directory, exist_ok=True)
        model_path = os.path.join(directory, 'model.pt')
        state_dict_path = os.path.join(directory, 'state_dict.pt')

        torch.save(policy_net, model_path)
        torch.save(policy_net.state_dict(), state_dict_path)

    def _get_tensor(self, x, dtype=torch.double) -> torch.Tensor:
        return torch.tensor(x, device=self.device, dtype=dtype)

    @abstractmethod
    def update_post_episode(self):
        pass

    @abstractmethod
    def update_step(self):
        pass

    def get_episodic_reward(self):
        return None
