import math

import torch

from .dqn_fixed_target import DQNFixedTargetAgent
from .double_dqn import DoubleDQNAgent
from ..utils import Memory, Transition, QTransition


class NStepAgent(DoubleDQNAgent):
    memory_type = QTransition

    def __init__(self, n_inputs, n_outputs, module, **kwargs):
        super(NStepAgent, self).__init__(n_inputs, n_outputs, module, **kwargs)

        self.rollout_length = kwargs.get('rollout_length', 10)
        mem_len = None if math.isinf(self.rollout_length) else self.rollout_length + 1
        self.temporary_buffer = Memory(mem_len)

        self._max_prio = 1

    def remember(self, state, action, reward, next_state, done):
        self.temporary_buffer.push(Transition(state, action, reward, next_state, done))

    def _recall(self, batch_size):
        batch = self.memory_type(*zip(*self.memory.sample(batch_size)))
        q_batch = self._get_tensor(batch.q)
        state_batch = self._get_tensor(batch.state)
        action_batch = self._get_tensor(batch.action, dtype=torch.long)
        reward_batch = self._get_tensor(batch.reward)
        next_state_batch = self._get_tensor(batch.next_state)
        final_states = self._get_tensor(batch.done, dtype=torch.uint8)

        return self.memory_type(q_batch, state_batch, action_batch, reward_batch, next_state_batch, final_states)

    def replay(self, batch_size):
        if len(self.temporary_buffer) > self.rollout_length \
                or (
                (0 < len(self.temporary_buffer) <= self.rollout_length) and self.temporary_buffer.last.done
        ):
            pass
        else:
            return None, None

        rollout_list = Transition(*zip(*list(iter(self.temporary_buffer))))
        state_batch = self._get_tensor(rollout_list.state)
        action_batch = self._get_tensor(rollout_list.action, dtype=torch.long)
        reward_batch = self._get_tensor(rollout_list.reward)
        next_state_batch = self._get_tensor(rollout_list.next_state)
        final_states = self._get_tensor(rollout_list.done, dtype=torch.uint8)

        discounts = self._get_tensor([self.gamma ** i for i in range(len(self.temporary_buffer))])

        max_target_q_n = torch.max(self.target_net(state_batch), 1)[0]  # type: torch.Tensor
        max_target_q_n[:-1] = reward_batch[:-1]

        targets = torch.where(final_states, reward_batch, max_target_q_n)

        q_i = torch.cumsum(targets.squeeze() * discounts, dim=0)

        # for i, t in enumerate(self.temporary_buffer):
        #     self.memory.push(QTransition(q_i[i].item(), *t))

        self.memory.push(QTransition(q_i[-1].item(), *self.temporary_buffer.last))

        self.temporary_buffer.reset()

        return super(NStepAgent, self).replay(batch_size)

    def _q_update(self, transition: QTransition):
        return transition.q.unsqueeze(1).detach()
