# TODO List

## Deep RL stuff

- Get support for `torch.multiprocessing` to seamless Asynchronous algorithms.
- Models that need to be added:
    - A2C, ACER, A3C, ACKTR
    - PPO
    - DDPG
    - HER
    - GAIL
    - TRPO

## Temporal logic stuff

- Support `Until` operator better for Filtering Semantics
- Implement `PSTL` to allow for changing STL formula parameters during training.


## Logistical Stuff

- Enable package use with Docker (+ GPU support in container)
- Add a `conda` environment description.

