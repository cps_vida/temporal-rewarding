#!/usr/bin/env python3

import gym
import torch.nn as nn

from temporal_rl import Session
from temporal_rl.rl.agents import TLAgent, DoubleDQNAgent

from temporal_rl.temporal_logic import signal_tl as stl
from temporal_rl.temporal_logic.signal_tl.semantics import FilteringMonitor

signals = ('x', 'x_dot', 'theta', 'theta_dot')
x, x_dot, theta, theta_dot = stl.signals(signals)
P1 = stl.G(stl.F(x_dot < abs(0.01)) & (abs(theta) < 5) & (abs(x) < 0.5))

if __name__ == '__main__':
    exp = Session(gym.make('CartPole-v1'))
    module = nn.Sequential(
        nn.Linear(4, 24), nn.ReLU(),
        nn.Linear(24, 24), nn.ReLU(),
        nn.Linear(24, 2)
    )

    tlmonitor = FilteringMonitor(P1, signals)
    base_agent = DoubleDQNAgent(4, 2, module)
    tl_agent = TLAgent(base_agent, tlmonitor, horizon=10)

    exp.run(tl_agent, 1000, render=True)
