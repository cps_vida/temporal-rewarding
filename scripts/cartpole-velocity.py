#!/usr/bin/env python3

import gym

import matplotlib
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F

from temporal_rl import Session
from temporal_rl.rl.agents import DoubleDQNAgent, PERAgent

matplotlib.rcParams['text.usetex'] = True

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.l1 = nn.Linear(4, 24)
        self.l2 = nn.Linear(24, 24)
        self.l3 = nn.Linear(24, 2)

    def forward(self, x):
        x = F.relu(self.l1(x))
        x = F.relu(self.l2(x))
        x = self.l3(x)
        return x


# NOTE(anand): s = [x, dot(x), theta, dot(theta)]


if __name__ == '__main__':
    EPISODES = 2000
    print('=================================================================================')
    AGENT = PERAgent(DoubleDQNAgent(
        4, 2, Net()
    ))
    ENV = gym.make('CartPole-v1').unwrapped
    EXP = Session(ENV)

    EXP.run(AGENT, EPISODES, render=False)
