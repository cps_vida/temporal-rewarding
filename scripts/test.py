#!/usr/bin/env python3

import gym
import torch.nn as nn

from temporal_rl import Session
from temporal_rl.rl.agents import DQNAgent, DQNFixedTargetAgent, NStepAgent, DoubleDQNAgent, PERAgent

if __name__ == '__main__':
    exp = Session(gym.make('CartPole-v1'))
    module = nn.Sequential(
        nn.Linear(4, 24), nn.ReLU(),
        nn.Linear(24, 24), nn.ReLU(),
        nn.Linear(24, 2)
    )
    # agent = PERAgent(DoubleDQNAgent(4, 2, module))
    agent = NStepAgent(4, 2, module)
    # agent = DQNFixedTargetAgent(4, 2, module)
    exp.run(agent, 1000, render=False)
